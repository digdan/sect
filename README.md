# SeCT
A custom curl wrapper toolbelt for requesting content.
## Methods
* reset($hide_cookies) - Reinitiates the SeCT instance and redefines a new random user agent. If $hide_cookies is set to true then cookies will not be used in this instance.
* set_proxy($proxy) - Uses proxy string to set proxies
* set_referer($referer) -  Sets the referer header
* setup_cookies($cookie_file) - Turns on cookies and uses $cookie_file as the past to store the cookies txt file
* get($url,$vars) - GET Request URL and use the $vars array as variables to pass in the query string. Will return the content of the request
* post($url,$vars) - POST Request URL and use the $vars array to pass as post variables. Will return the content of the request
* set_url($url) - Preset the destination URL
* nonseq($amount,$iteration,$seed) - see [nonseq](http://www.danmorgan.net/programming/non-sequential-iterating/)
* debug() - Will return full CuRL information
* random_user_agent() - Will generate and return a random browser user agent
* encode($string) - Will custom URL encode a string

# Sect OAuth
Sect class is also extended into sect_oa , a 2-legged OAuth library adaptation of sect
## Methods
* oa($consumerKey, $consumerSecret, $signatureMethod) - Initiates the OAuth library
