<?php
/**
*    SeCT - cURL wrapper to help facilitate HTTP Requests
*
*     Features :
*        Random User Agents
*         Non-Sequential Iteration Engine
*         All features of CuRL (proxy,https...)
*         Custom URL Encoding
*/

class sect {

    //Base Vars
    protected $ch; //Handle for CuRL
    protected $return; //Copy of data returned from request
    protected $info;
    protected $error;
    protected $proxy;
    protected $url;
    protected $headers;
    protected $referer;
    protected $buffer; //internal buffer
    protected $cookie_file; //Where cookies are stored
    protected $last_post; //Copy of last post array
    protected $return_code; //200,404,500?
    protected $return_message; //OK,Not Found
    protected $timestamp; //Request timestamp

    //Construct
    function __construct() {
        set_time_limit(0); //This might be awhile
        $this->reset(true);
    }

    public function reset($hide_cookies = false) {
        $this->headers = array();
        $this->ch = curl_init();

        curl_setopt($this->ch, CURLOPT_POST, false);
        curl_setopt($this->ch, CURLOPT_USERAGENT, $this->random_user_agent());
        curl_setopt($this->ch, CURLOPT_HEADER, true);
        curl_setopt($this->ch, CURLINFO_HEADER_OUT, true);
        curl_setopt($this->ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($this->ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($this->ch, CURLOPT_PORT, 80);
        curl_setopt($this->ch, CURLOPT_TIMEOUT, 5);

        //HTTPS
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($this->ch, CURLOPT_SSL_VERIFYHOST, 0);

        if ($hide_cookies) {
            $this->setup_cookies(false);
        } else {
            $this->setup_cookies("./cookies.txt");
        }
    }

    public function set_proxy($proxy) {
        $this->proxy = $proxy;
        curl_setopt($this->ch, CURLOPT_PROXY, $proxy);
    }

    public function set_referer($in) {
        $this->referer = $in;
        curl_setopt($this->ch, CURLOPT_REFERER, $in);
    }

    public function setup_cookies($cookie_file = NULL) {
        if ($cookie_file) {
            $this->cookie_file = $cookie_file;
        }
        curl_setopt($this->ch, CURLOPT_COOKIEJAR, $this->cookie_file);
        curl_setopt($this->ch, CURLOPT_COOKIEFILE, $this->cookie_file);
    }

/**
* Send a HTTP GET request
*
* @param mixed $url - Url to GET from
* @param mixed $inarr - Array of values to send in GET request
*/
    public function get($url, $inarr = false) {
        ob_start();
        $this->timestamp = time();
        $this->set_url($url);

        $this->return = curl_exec($this->ch);
        $this->info = curl_getinfo($this->ch);
        $this->error = curl_error($this->ch);

        if (!$this->return) {
            echo curl_error($this->ch);
        }
        $this->headers();
        ob_end_clean();
        return $this->return;
    }

/**
* Send a HTTP POST request
*
* @param mixed $url - The URL to post
* @param mixed $inarr - The array of values to post
*/
    public function post($url, $inarr = false) {
        ob_start();
        $this->timestamp = time();
        $this->set_url($url);

        curl_setopt($this->ch, CURLOPT_POST, true);
        if (!$inarr) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "");
            unset($this->last_post);
        } else {
            $this->last_post = $inarr;
            $post_string = http_build_query($inarr);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_string);
        }
        $this->return = curl_exec($this->ch);
        $this->info = curl_getinfo($this->ch);
        $this->error = curl_error($this->ch);
        if (!$this->return) {
            echo curl_error($this->ch);
        }
        $this->headers();
        ob_end_clean();
        return $this->return;
    }

/**
* Captures and stores header information from last request
*/
    protected function headers() {
        $this->return_code = NULL;
        $this->return_message = NULL;
        $tp = explode("\n", $this->return);
        foreach ($tp as $k => $v) {
            $v = trim($v);
            if ($v == "") {
                unset($tp[$k]);
                $this->return = join("\n", $tp);
                return;
            }
            if (substr($v, 0, 4) == "HTTP") {
                $rc = explode(" ", $v);
                $this->return_code = $rc[1];
                unset($rc[1]);
                unset($rc[0]);
                $newrc = join($rc, " ");
                $this->return_message = $newrc;
            }
            if (strstr($v, ":")) {
                $header_parts = explode(":", $v);
                $key = trim(strtolower($header_parts[0]));
                unset($header_parts[0]);
                $value = trim(join(":", $header_parts));
                $this->headers[$key] = $value;
            } else {
                $this->headers[] = $v;
            }
            unset($tp[$k]);
        }
    }

    public function set_url($url) {
        $this->url = $this->normalizeUrl($url);
        if (stristr($url, "https://")) {
            curl_setopt($this->ch, CURLOPT_PORT, 443);
        }
        curl_setopt($this->ch, CURLOPT_URL, $this->url);
    }

/**
* Normalize URL
*
* @param mixed $url - URL to be normalized
*
* @return mixed $url - Normalized URL
*/

    private function normalizeUrl($url = NULL) {
        $urlParts = parse_url($url);
		if (count($urlParts) > 0) {
	        $scheme = strtolower($urlParts['scheme']);
	        $host = strtolower($urlParts['host']);
	        $port = intval($urlParts['port']);

	        $retval = "{$scheme}://{$host}";
	        if($port > 0 && ($scheme === 'http' && $port !== 80) || ($scheme === 'https' && $port !== 443)) {
	            $retval .= ":{$port}";
	        }
	        $retval .= $urlParts['path'];
	        if(!empty($urlParts['query'])) {
	            $retval .= "?{$urlParts['query']}";
	        }
	        return $retval;
		} else {
			return false;
		}
    }

/**
* Non-Sequencer - Used to iterate through lists in a non sequencial fashion without repeating
*
* @param int $amount - The max value to return
* @param int $iter - Specify the step in the iteration, should increment integerically from 0 to $amount
* @param mixed $seed - Optional, can be a value or a string. Used to recreate non-sequences.
* @return mixed - Returns an int between 0 and $amount
*/
    public function nonseq($amount,$iter=0,$seed=NULL) {
        if (!$seed) {
            list($usec, $sec) = explode(' ', microtime());
            $seed = (float) $sec + ((float) $usec * 100000);
        }

        srand($seed);

        for($i=0;$i<$amount;$i++) $map[$i] = ($i + 1);

        for($i=(count($map)-1);$i>=0;$i--) {
            $j = @rand(0,$i);
            $tmp = $map[$i];
            $map[$i] = $map[$j];
            $map[$j] = $tmp;
        }
        if ($iter > ($amount)) {
            $base = floor($iter / $amount);
            $niter = $iter - ($base * $amount);
            $prec = $this->nonseq($amount,($niter+1),$seed+1);
            return $prec + ($base * $amount);
        }
        return $map[$iter-1];
    }


/**
* Returns curl information
*/
    public function debug() {
        return curl_getinfo($this->ch);
    }

/**
* Returns a viable user agent
*
* @param mixed $seed - random seed
*/
    public function random_user_agent($seed = NULL) {
        $browser_freq = array(
            "Internet Explorer" => 24.3,
            "Firefox" => 42.9,
            "Chrome" => 24.6,
            "Safari" => 4.1,
            "Opera" => 2.6
        );
		$max = 0; $rcount = 0; $browser_type='';

        $browser_strings = $this->browser_strings();

        if ($seed == NULL) {
            list($usec, $sec) = explode(' ', microtime());
            $mseed = (float) $sec + ((float) $usec * 100000);
        } else {
            $mseed = crc32($seed);
        }
        srand($mseed);

        foreach ($browser_freq as $k => $v)
			$max += $v;
        $roll = rand(0, $max);
        foreach ($browser_freq as $k => $v)
            if (($roll <= ($rcount += $v)) and (!$browser_type))
                $browser_type = $k;
        $user_agent_array = $browser_strings[$browser_type];
        shuffle($user_agent_array);
        $user_agent = $user_agent_array[0];
        return $user_agent;
    }
/**
* A compressed string of a multidimensional array of browser user agents
*
* returns the multidimensional array
*/
    private function browser_strings() {
        $ebsa = array(
        "1X3bchw3kvardPjKit0uFYBCHaQriZJsrUWJv0iO5/eNokm2rB61SEaT9Hhmw+++VUB1dwFIAJmoatoTsbExosn8PqCAPCGRWDyTz/737hmrnn339vp+uble3s9e/367",
        "vtksN989Xzwr2v+8epY/v3tWsWffHd/8e7VeL57KLJ99f3nz7XZxv7pYL5/Pjk/fvp6xPMufz35eXV/d/PNu9v5sVmas/feHn8vi+exss7paXt8/LbP8yXfPV89YK7Is",
        "0kTasvgEsmQvS1iyihGyilZW0exlsYzzqDCRMfXHsv1jWVuD6n/p+ey8/9tG/enbHQ/17+X1/Pz0iZJSdlIqnJQVJKUTUnVzImLzawupoCmpW1FMkGVBK0l2v3P67uiI",
        "t3+0vFotZkfLbgnPTo7a32//29vrzzcni/svmeikzj+eHc3eHc/q57NfHq6XsyKrFKNGMeJ/DUZqZ3S7jbPJ5yh7//psdvTu44xneSbzileDn4lMZqL9WWP8LN/+DBxN",
        "Tzo0rk5W+xtHu//1Wm/+bvczGdUo6CFixwHNATA2TZIrkmUaydV1R/L3x2HaKS3O8wMxHTCwl9K4ZXO2aLndd/+B26vIt3A6lVpFVT1loEpsp2xrW9nixBpzdvllc/Nt",
        "+Xmz+LZ8ylg7EWVTZlIrGlZOAvLEBMk7kKLOeC4VSJUIklNG0mnx0t69M9qm1ZIay7TA5tYWKSHTwju9yaqosCpipw638qEFrqkrfTj8bDjqj67y3W35fPbi7uvZS5lJ",
        "PRKlNMvmrzSS3bRzr17hQn0AmUb7sTWofx0VynXIk4bxKAvozavjwW+zjGWF4OB3scyDHl+npxmL+g21oy/4fnzK9Njkiu2gmTWYvOXXg5c439UFZxY4hFPIvMw4k7G1",
        "sZ0yTanT9qyIuihxSsM9Mobebp1ofsrdz6POdZyfZ8lAmMqhL+yYB4GZ++akJXP6t9QZ0Yuo+2XRR5jKTskmRtDepC7BNy9Pj89+fmlNhSjq0iYrqqLF56y2yYqyG8C3",
        "u+vZh9v71bfVv5dXb1/Xz1+/70M/4XjqsEaxZ7N0dtxhNIomqYwOj1p+l6RrdIZcw8qXG7GOJqLMCOd/JhHFo1BWOOpPUHkggxJalLOzXkJp93ry2YNYh2MNwpK0V4Ay",
        "EnnU/5l0DNoYiOrxPvr4AFBs964yEXWCgnlk1o6O/OXHo/d6CI0Kv6u//BCM1Ewk2jYCwLojIHnGeaHymk6G6j9hwNA23+bdhtOhU7ed3eP5X39Zooeoh6UsZfUfNSxN",
        "XAdo0cjmTyc+3FbbTJceQfEfsmliptGXSv/j7lmrAt+sNsvPN7+rs6Oi2B0e2SHK31k7qner64ffZ7/X5aducJvfnrUaZsFuN8snsx+Wl19vnrYbkOWCF7Neajtw/Qvb",
        "E6R0uXn7fwyU222RYSZgeGQCxvxU4mI6AAECFJSZyS8aW24bYQ/k6l/YHknRiTsAeesh1yBA5zxVdoBtx4Ve1rkEhaqMqJ0b92ywXnZtE2asMGTXvew6cUYcgFYxgADN",
        "VADMmvItgDptmgbBM0fqsAf7VQHeFSxVbVNb6vHicnV9f3P3pVNj98v1rP3B7MPp7O/dIWu5Rwkogva/7s5UQqzLAevK1i0NN0VWW9bg+UV4MVbWLFuSd+cXY8Q6c1Dt",
        "TyxoW6d0piI3V3a5nYpuXzYx4RSpMEy3RSsbxq8MCwOmZuauKXaHFs5sD0Suynq/1oRNvRLmVIut3VGGMrATVRazl8qEYwxyc/uxnVwWlcsCcjkXHrk8YmaMaWDcEWwT",
        "5lvBYoxgdy3vBXe7r7azkeH1zByra2tStnMclH30FFuEVCmAYa2RPUY5HQZvPBjVhBi+uaoji3K4+yG5pUdutzcb+4AAaRYIOCqJmzRHuettWXOUbzFiG9eYI0Cu8Mjl",
        "EZVo7q88vLnyXeLTOXYe1hnZtFV1EQwCTHu+y2qWdt7cOx8YidKrac5t+3C7np+824q3NK/houzSgM58mIL1LLfz8MNLUGzR6cfl1c1m8ZSV86vlbw5IBc2HAnmxufwC",
        "fEwremCORH1e43EkwO+4eVDCWdZkPBPGUinMpTL73g1L9/kzp74M/q7mCFhjj0ClpjCyWsKhdb3LATXg/Fofcb82unnIMz4QzVlnQc8vHq7vH562kyRn3/9j0f7jX092",
        "eCKr98mZ3FZeRMC8br1PwZCQAg25up+/PZsCUp1NOJvOs8akqSt8y+zx2Eso7IAQBxtDLKTto8iBPhJZpX5ByS8D4Vh0A17IwbAaNvSwRFZeqAqhAqwQwqlqPEgoMAa/",
        "8ecNEUEZ+kD8d+6x9dZoTKS9KZ7tkKQ6r80pC9YYTGFAMG4OpvCqRckIHwraJzFkBcKhaaSsBmaA5MwEYbMfzl7qaQRdBNjWWhhW5qsN/oqBX9nC7DJfspgSZJBQMkDU",
        "OWVuO3+wEph/PI9h7PROu84BvTMALiHndvexnOVwN38dGSCHcSoaTnwiPTjK3WCegAxc4v9YUIH8+0vl1sLm3bNWLN+hYbL9Z6t3boboWaNSmjkUc25R3myWy5enr2Yr",
        "UbuqyUERgHhVnyE8aRR3A+dDjxNCqU0tkTWz78PHCUVnTXOZl/q+AScpLU6mo0BEaE4/3C6v/XNqpP4YF7khXtn4skhfGGZmsW53NLAwNIqMe5c4lKr1L0/PT19r2fM8",
        "YxkD8Eq0q/fvL/Oj9wE4vlVZXfVxq7PWD5erqycApNIjmCjIiCLc0SHhavQIP6/mb95OAdlAGVJPnBf8hJUrvAJzDJDwq+X81euAcFA9aQyGV08tytGPAZQGEE9RB8q9",
        "iK89F6RTBw3FZ769n7/8iAfpfJgqYwqrAG1/CGx1H0TqyqcvXrx6cdJiNd3WbeMZaCZDh28g7pclcRqVd4E0xqXt5voRZs4cqkIl57pKxOk0jETsg/lt1q7arKKEKIrE",
        "FWpCZ/sJDR3exRGGUW6XRAB0ep1TrW27vc5+pq2LmhyHJOzhWuU+GCXIuketPsyCeK0YCLBWGWeQK8vsN9vMXYdezVn2+dIMjPRVv7qYDLOzWS6mADAlmOPD2CwbsVSl",
        "Ix9OO+FzkS3XMrvs7MwdgEpWLl8e/Mj2uAbKpa7I0XrQ+eAWki+aqGu78DOK21qhkzPCIL3QquKdhb0Q3yoqDdCyjaujnk85mO4m/1OwVX62UbEPD6/kSbF9X6DRmdtw",
        "WnBKJgqUfOZiLzgbWQIgI/2dCASol/WUSsp+gjJTcWwfdEnyJ3PX4MWQB/unIvkILdb1mvgFdZIF+QXlJF/Qul3XNCO2h1mQUbZmNLo9dJVGjg6fIpBw+NSDMFD/RSIc",
        "wwWzB2j6eYV/i7SOw5jDAgtYWvamH5+g2u1hhi46tv1GaD0SiuFWUJd3+NF4dzvLwcvflO2OmsiSpkjz4EwCw/OvkoqMbAR2FjInIJN8om1yMrAVpZUmCX3VhvpVidC6",
        "tBF2f8bA4KdXd9GYcnrxm4al5HmPXuDHrVEE2XZRtat2J0OzXJAde6J+CMyypCwvDHZOWF7OVbYo+PXF/P0HimnzYlPcIgX99Ya6tshH1AFz4wxNQzQpiS6z1MEuHLAO",
        "JdtfWD/8urpmgQ+pCkuRJw2G5rfrLIwMg5gXboZBaMBgUjns7gGYrnx1aI2L+7wpNV04sjs3EXP71KTHwlfIfL2Z//TRj4Y5VOgxCzRmbPoQ3nKPCRavpkBaszj08ThY",
        "UI4B4QaIYCU0BPRxU2hRED6Tdm9sFWICvjh+hVvme1DWCvm2+G25WV1+BXEbcmhsW4XYN/MqZhG6LjKmQHA/PFX2SoK4XVMhyIGTERKjIMiB0xfCRA03lSqLJR1/WYUh",
        "JDBJ8cjS0UIeGVxcOyEFjVKNTkCl77KanHUznJMosH9uG3oUvphiyKpwt8LYH8NFCdsFrkWzUQebEYhBQprpCl5SNihu6NQB8enJEfceEPfDFKPDwdhQ/V+PlCZ2T12i",
        "wN4FW8AtoYLnLmva1/Vnipkq16WhG74HadzbPkJlUffoFTk8HI/edzFiqsTXMTuxAtyKvOg0mFZLyAsVhnj7YkxXJ8j3NZeds/V1sfm2ujRdrd0t8778F1dT+o/F/H9O",
        "huDD7Lm+sI3x81ipoXVyOexgekbtBQ4MWc/8yY8nT/NMd5NU5cGRSM6+xwITsFTWdoQicI0qrpqRIAUJJLc0BBJE6yFKtsRIWURRMActTNJzNrQJDWhiGbo26KlX30G3",
        "Zs10HRqrXmI7zfSSYcTKVCb2/Oxji8RY1v4/KZschG9AeNyOhG7sorTB9nIIK/N0hSAtbESYy6SjEHS9ceQoDwiybXiRD2tHmK92hPWoKuFDcqssjx8YPoSjXChBuf+G",
        "mGdrSiO1ae6UK9+qQJMCSrwBUmrJvzwrAks+SlXToztgCXPmUzol5aKlCVqEGogo3GK37yqwyt6EaS3qb9UaAjJQuFXR3uq545d1mT/923Jzt7q57lpLivaDqB9mL751",
        "CmFxfPPbap0tr7Pjv2tCNXVD2BkgmxWrLVYapyFvvOv5i3MyTpWTraezxmNAfRojYMF0MbS10dJc686aGfA6qVBxRMrQvXG8lzm8u5CXCBXe4woCrn2KMwZX6S6Oq/ww",
        "cmxpoH5FUUlCqaXpEwlLSxjHIExA1Z3b4SvtFM7jxzG70QtQPHhPC3fOYiO0AQJQ5LIFqqcDat2tAFAzoiAWWDIfl1ezHxf3uwJV8CvpKuq0MkIAE0JghBiyhXj/C3Yn",
        "BLzGHpqicRyd6oUORJE9LrGcOoY7/JK8cxdB0IIwz1eL+aufpptnpV94eJ6P356cCj5jeVX89Hl2dHI6+38Pi6ujm80ysrJoaqcu0U66kYUbOQMVGtW+f+MAN9BYQR3Y",
        "111jUJ2YYMTqxlQ0QmGQO9BBGMS9YZBG1cXWE6CSvqsqs/a2PHi/vPdftmRGbq/z7E0AndxrSCrqbn789wAErxB+Qo9LcYqcxePiDpcsB5dsj0u69RHDLfc1DC1qnlVW",
        "GcMWVI4w4SZmCwqa8B6oHGFNXSAIoULM3+3tpe1ThVZKYBf0oDV4MIapAxkJrFNOKAVnm9FU4MGJL8+10sEUbZhKJ3mDcFVU7bw3qBC3N1LNrn1G5pDl0AbguX4kgHIM",
        "6MTM7t6z5g2Ro+W6pJpSMnr32/z0NYWHxtGNdpCJGfA8EDHgSPth9z0zuCWxzHiXGulnCLwaj+jtEl6ADvltaxLeF2fjLIHpMjELpMYZ9H5vqZR5WKcM0krG5nb7Slpl",
        "jluMmpSW55EBMg6CkEqS3EsmURR/1oYzygGds6VN3dEYvQ0CKjLX0JS4znF6x0FzCrSdSRoHLRCnEF4Tnwo9tEmMEuct1/MfJpx3iTgBAotPpxk5RVNd3s2PfgkwYBLh",
        "K/fDJgR3kcVGQa2xx8+fN/M3oY9sB8q9fLjayRM8vphuFXF8JBfbQCzeQGqHyvDDtV1JW0EjCxl6YE4BNq6JjJxnkRgxj0MtEl3mwKf1JiR6SDk1ZHRzqmLuBteeJrwz",
        "9wFs3gawwg5gezhKeYGuPL4hKwROvLHGrPu4UZSAH8PhGid8MUwc2wetq7pT6vKZGXq1++TzEFNLB17q8A1LPXq3lR4K67RkoAV52LVFSxb4OzPejpKtbI5olqUBC6gk",
        "nQ7IRA4kZzSECqtw3Y6HW9asz8vj/Qk0WonI0ngue9ihjWA8auQ0aBXKVQYwrWVce+ewxiq9IABCzWo48BZrCtxA8qDCRJ2+c900GfmgQXTrqHLrAvwGu35irTx3sLp6",
        "mpNUvNlf0+ImZ8cfX7Qbv5x9f/GwWl/NcsGr2gD260RVTu2c3YbIrO9x86/Fk66VAqVGYuFkAMT+9DE8NAmlHShVTnabw9IdH9xuMAJiZjhCdwmeH7/+4cX5ybsPL17N",
        "mC6/57q1MQVysaF8spokPjczKXHx8Nl3mnyzlMm/FCQciGCLY4Nqxu/qeAqb8TVj7ELavV8lG17QlxdFqPcrl+AVUgqBwm0+2zgENJY4ABbzYBVYT+56PX//bohhAhjJ",
        "RKmbRfO+yJlYimgOJQyjcrKBz0aqbwZqIeME/NikC2HAZfwx2DXhylsKtEahZ3Gpn9c7wpLWytAxF7gRluBVsNjJgjQiLSQSBwNyBFTKjtGQ+vVp3BnvIP3OrJ7LRfx+",
        "jOz6Qns/ZYGPnQI0uhrx3YlhCzjn3YEhN1loQInI8gB1dg5ia0L6XESHZzdS7ga9v+zHS0yVzeAY/+jdyBnXqBV+cs38L320GlBFPJgQZJAJcQfI0ANs0JmsLw/zH8/x",
        "64f51o+qPnaussVLiOIzCoFhzoygmC5tToeHBxX+gZkwtIhnJnfDxffPsDHN5yvyfWPEKGYBaflQHHAfUgoyMKMSTGwjr/nHobw6tiK/ExPUexKaRVprws4iB5Q5DEFx",
        "a4BAFImiNQrlAmBAlQEfyRvn1DndX45oGGAxBvxlVXjsf8kDwA8XDgp76FfLHoiDVUpIJOSeMzPYXWlJyeoen5Cv3Qx3oJnJ4N0d2L3NqCGbUWvA9CI+HCiDQFXiJiUr",
        "bWMKBqQ4dzAl2gRbYUb6dFZoSPOO9QhI/HMR5uGvC1nELVOP2WCdKWN3JOM16K4aIbwcmr6G8HJE+JOJXELyyT167tbUIcABEyGdEYMBNbOTf1cFwdhHuIAuh3EaXRcL",
        "DSUpetp+cgkFZOtpwyA14Pu19GOubs0A+qvSIJhiFV+QZL+LHb0UsEPFHw1Z5qAyHN3uyHAXRXR9/O24rIejXc3MrQ9pY1oQuxUjdP1vSWlRbeiSGNB+qXQjLQTng8JQ",
        "clGpflCU1pVZnQwb7jGes9uVRZC6MusWDIQPoyH0PfX0O9cQjD4rk7uzstbF4U8CAzdnHFPyAqZgSpMIJrLsF+YIZ8hsas7auB1QJj0MpuIOdobsoRmeSQm5eeVg32Fu",
        "OcD6xMLdt7nUqLY+GYKCxcKYYN2eUdC97EEIVXdmjjdpsexH11cKYz6locJSUftMoVBlwnSvL3lnMER/1DhedNloLIF23G1Lay2ZvAK2B4cgC+x0Xi4mmU7y01tGVtud",
        "VgCipBgsMBMSgfFnKQQjPZmjLPzqioLsBa4pZgz0v11sbcaKvRkri8rem35GSjX53pgHDauRKsJPBcHZUe4F73pxeT+hrice5RD4ZjLkEARmkhNdsKkYaXDSXS8kuGtU",
        "PNHdy9Pjs59faiL0BvbkfR3lkOyamc9ws7yJa0ypIeG0ODJRZRU7tN+xQgOX6T6hNVoGPN26g6niMJ0Fmn2vwZ5E0axAuochP1H48JUKAb8dRjE8Lkx8m2pwkZMT80bu",
        "dgQy8dket44kNrsBe6urltE2hruBxQ4bGNn40DBpWv2WQFU2j7MEIz60hBKgNt65muLLbTvCOmOIrSPgLDmiwuN2TVhHQ1WuypuRFdxGtsc6SmXl3rUvlGtvohYajfRy",
        "GKgbXFwDJm684ALo0F6JUMgrYKRFTi58MiuP00aqsfUrhsTtYaiiKHb8VrooyFkrI3ZEzTLmMtZUjojoKyJFX2id0pCCDUuQ80bfVVovLr/+c7FZPmWtb/7p7n5xsV6a",
        "yBpWxq8uYlEFJL/EBsGGyRTmd+I5YiY1XoU9STK+HGooddrtI1d6A0lv4ifDOOlA15MWZHCaowEl+EQGJrtlAtZGo0ipX0bQpkcSzt5sRYX5InLks+3RoWgUQXP/7CKW",
        "2HT5PTBJf2vQ6FUcRfYCw13CKFcvQrciWizrXkRoEsglRIbGR33hirqOrpbzV6+pKHBrnpAzP2i9PeZr0h547x5WXBHHpjs+YxNSYIoMPzz/aVvJaqhdiz8hVcJXzUO8",
        "L36d4pv0ddeU6Cp9wmxsQRm03QN+DHCBT3sYwQe3jAHb+zcGNtcwEqqrxPk0JlKdizAS+Fx8KpIhf4+rkSqs82EbUhungMYBPpKBcT5M6e2XtjxPCKxJ9aNcMFe6KpKO",
        "9Cqycl6GtUZhEF80BRyPCIx/D1UkxQFcWByDLch1ooFt7ALHOl+LingbNbcbMY4ZPPn9ZeK8a5SSbJAMdyd9eOQX3im4GiLldphRP4ZCoXg70slGYyBq8iUws6kWEoXk",
        "m7g1cMlLoSbnWCi4GoL0CJh0n8oYMzy4JJqSLzTeZskZE6adY07vAqH7LpO6phj7C0BUT2Mc//+jp/B7a7J/DEboMmnsLWZ9bn5PGuxWb3tnXJ3cU58Gic14/AQ99hrR",
        "UU8PrIikJP6xU6Th1FkW9vUWLJ5aDSfvzv4mW8i6jYYYF5avxwLn4LpnMxtxsXWCdaJqtakfIomDhuOEuxHmlpAGjNE0tkWRUFGQfrtHNPhWX87gRqBS3haTzglm8NEr",
        "2XfgFg0Yd+F3eW70Qu2UuvVguex7xoqG/n6YOR6zSrsLYbU2qfYtWCopn4DY8QwzsGHNBlQVt9ZlvyD1SRbFtzeNhIViJd087UltddjAj4dR3Js0Igq+UIXcpPfZjKuK",
        "UWgNog/Wxxw/W33MpK1s/XquUKXXyFtg5ge2MQcHpKopnV38qPvFFboQG7+wgOAlNtw8+iByoYqvkc/M7HFNjdBqcP2LBrgWn3yUZWerpZXS6uWXI07ofMM4Xl3fP221",
        "zg/rm81q8QSCrdJP6FxUQD7iPghWfjFYjnNnLWq4TrtU0Za4SSuAoa+RGdWQNoCERDPEx/dkreyuOF2sEmkTt4MFQzEMamRQu0O6QlVGRx5CAKqwLfkcPHXsBwHec6ci",
        "YBaXKoF2Spa2SK82i19vrt+s/9UfOQ7RrNHkkHD6e+6Rb1JDKOTr7MFJA9cy/SDKaDwQW1peu8dIjXuANwVGYHPwuDsEbSQ1MPPa1xKnv4CJ27WDMbZOW8Y7N6AfI39s",
        "AvYkC4pb5WYRR6IXvrKzcGBlgJa8iCpnjabcDWRbHVMRmIBmjLq7BlToxsUYxTxcqc5ghrL9U6fvVmAuma/u52/PBjHbMGSr84YLJpAT2KmhChMIDw9oQuPTYpu4PwM1",
        "Y09cBqo4OKHTqrXUh01ygnAM/aFavIe7KT6UKgR27n3H7LdvOmuF9WWxuYKgBHZNBKEcsUXch7YOBWnyZcipNQudwstAuLKDj3OGZOdG0OajDt/fDDkcwzO9NHWjS3ad",
        "G45BX8pM/yfiNnTckOo2k21+w1yAT3dOBKsRGPkzYld4YFgcWppTYGrx8MN3IW/COjBMWCV/3D1rPYijL5ubb8vvni/UHYX/XT3LoYyNmSJ6Mntxe7te/ry8+Gl1/1SK",
        "IuMtyE8/nh2/++/ZevV1qck8mWnhT1lXx1TlLe/Z6eLzYrPq/6Ybu/qaAoz7hp4FAFmAkMarEAp/9fBty6Ar0zA5CYuT7v+hOIG6ea9Bp2HEHUYcYhSsY9qtuZ8//EyY",
        "KQQs2DfSRJ0SDnxlbCQca+HKpoHgggUIu42WNqkB2Aq8xDG4peJcUfnU7oBEBjXEoD7YPFcQHNw/MqpSMHhlVggXkAUe80lSKjaoKABQFiqenmQ5eYADh3vTjBbABLu7",
        "TbKEYLy4JsonxZP+4CPBBNiIDPqMZXz9JOv4HTAHgCvwQPxACsnDoT6YG9DGYHuju+WQO6wAvaWTeDRNXX1KX4eQJxLzxtL3WQHiMVBXx9e9iCPW9k5TZ+l9Jo9YjnF+",
        "CnBgCA6239E/iPSYHCrbJmsOqlbLeWslcnm1W2+HICMpE7IvcAA45AgOPDNVQt/OG6rU3sNOilUlTX6rkQ4x8Jo2+YHViOBQ2j6q5tBMtSNYg+DATA5MtQkWSYVaIIc6",
        "gYPqSKmTkJMuDBSZHCRDUpWhhcGqOAcpLQ66051InJDiEGSKR52QAuRAUpX7602JHDjIoXykecg7DkUDcoC9yHCKbP7q9aQctOYknFQB6CUCvc5qQ3H39ccNeCjpfT18",
        "BDzPCxdfN01gvhtB2wT6NBMAM4DPhQMp/InxOVgAHtdOcgI6Ou8OVhR6D8mS8ZiAhq99SHLvoIlZSAqLoN0ew0LpRG6HTqMs9xg6+mIAoREWGt+TX97TEQBFAVCsEepr",
        "GuUB4yv1WaAPmB99gvTTfI+yuSF0UluaUEw2igVP3FbiMHREIp2AGzaGTvGn7XJeuRTbn7kU4RcDD7LLQfxUvXwgOlUSHfmpPgydx/Ngu9YCDnxDvpahLuFPSkJ32CCR",
        "mH4mdLuMgvD891Q7OXf45QA9pYbhN50PaStx5A6QI8B9SYhM8ed8SO7MlR26a3oyca7GRSzCjp81menyCBgO8PeqwN0fb+WaSMJO6mgSpBTs2BDGw0HpYh69PxJZuOom",
        "gcbh3RFTlA0vITYVxveNsQkXCSl0ZhXk6Fudug3JKHUHY4e2MBP2FvaQG6+LY+SMM0xNhWdW1KJ+4pLTCQl0D5rEr8YzBmAXf8ZXY85XszP7mhwxbxvY4uHTVsXBrndi",
        "uhJoOn2L4WDPg+ZAytuOngeYA3xNB69cinyv2QDs7sLF8ESl+3WF24AhdRpuZIX6WfRvUlKabid9gcb/AWqMfo1pd0r1o+aSZ7lJzj720uQ4ZX7SV2iIQuKrAQCFYKFE",
        "R0HaVYO650ANN+2mKFMEdJXVBYAtHwe7gLCn05EoDqwBOEynI3EcJMChnj5QSCfTgLffaPoChQ7shIaoL8d+DYgCm0xlJ1OYTismU9Bxvm22D78kIS5FIpdDLFD5mAvU",
        "Dtc0hXICgw6Dw1GJ5iJNg65/4JLTZ2HTm5Nh53SLGbeZ2Q65ZlYH9drwq/m58eg3q+1lo/rtNPCdccq0xKEr27ngumAerrVKyrogSNhrticBn3AdyB2vPe44y4maNXCU",
        "FEy3dK3LpLSTYD0HuCJ1OxVHm1aBqc+Rd5e/RMC+ICjwTFieV08CLq/ykOCBExEkCQGRgOP0w5FgEAnYCT0YCdFAJPT9Vd/J3QFIaNgacSwfMyXYbdkp7C0BLiqbU/cj",
        "YGLop1a+0jPUzHAO7RY2URgvsqDRB0kI3Zye6TtITs541GnmmCnhE5xm4vCtKKXHJzYDCzhiY1gUFBZBT30MC5m0NIIOMpIOZOD691UfY2nkIH412tlCoNvJth6b9IxX",
        "+oKoOg7OqWjPYYoIHoFuH21pdP06auRZa28rNrc56GEdNT0SOzHYj4SYCRj3LT0cSA5ruv0LciBmSA/yLQqw+jdhXwXvrCgKwtra2lfq701R+jh4GAQviSgG3GJQawb6",
        "FinmGTIPcrD6XiGzKjPNfaWhMSp1/5rc/M3HJHBLo/TYNQE7feAwdjPVsktl0N+UmoBBsFpBMbAPSMptIxH9M9VIpCy2jURoxej3m/mZsyhEa8F9/UX+ttzcrW6uO8mZ",
        "4Q53f6Lnhkzi6838p4lJcDIJcHuMIyGmuRswjgT9gsLl3fzol2lJkO4n9A0S5/9zMi2Jcppq7nEkMNdeT06O3GBRPc47LRfM+RbIpf04/7idlkuTWKNcHYSNziykXWU5",
        "wIfqkwxJdO5+m98tJ6aTeiOhVLr+62ZiOqk3EsrDrJ0inc7qfr66n5iOTKfT2sTPU3+sMZfE7ubLu4nppF1G2N5Ze5iaTj2Kzq8XE9NJ1cqqh+b8amK9w0ntCZQvo1o8",
        "uyxYk8GFSUMSYkii+wvFgZETnKNcGZgEnyb9Po4EqYWL8qe+PMx/dG6ujPoapETveJ8OJiHJJA7wNUhFW9J7j2jU14AvEoQ4HGBvkN65USRU1+lpSZDOx/zpgVEkdI4i",
        "oUCn8ti27mbCfyVyGdHVpfVCLr9MuU51V5e0KirQrHUV7qkTo51X+4gZxWUB+GYjZkXpUmGfT7y4vtrcrK5mvEsgJ+QuRhAitsRiKvb68SeAQ53BjbmGHLjBodZ9gqfq",
        "84KcB5gDsc9L7k3rjZgHYoeslsP1xfz9h0k5kDpk6bfm4aReOoeC3iFLtY2ecj30LV+mMPXp88CT9HkokzWCjNKhha3PVyc3V7pO5+R8tjr5cnO97IgUn8QnpgG21Pyc",
        "Ks/BlMnp+OZitV4+rX/YxS2lwU/fyrI/mJcfd/l9Wc5X69H8jloSMEOlaqV911CTUhy7ZzIQPKF0QQLJBcyy9MzjjiXuS9//82BfuiIztPndrue347/0G9bkMMN6NENP",
        "Vmgyhs1ohp5UyFQMdVcaaXtKAYbATmnN4/XNATeLbl4zkqQnHzkdSU791o+tc3S7m5HT+Hk1/7w6JEllYKR9tPVnkYQ5TtS1UTGCazWGjJjhKFS6VqPvhJPUJuO+tRtQ",
        "FJpOpqJdwXXjcZbxaNS5R+5+W+ESr/5eLibCJT39G8qcejo8gOjCbHFDeianexUozU8F8Inv9wUCyTR8Tpt9/xF80uzDZcHpR81JJAqwYC5MovjEmMfEpFDQh3G2NUH1",
        "cvOFKCk0ynQabSi5eZiIRpVOw3OOnESjTqfhc5BSaDTpNHzpWoKaFkbbmWQai4nWhu4/k0hjPf91orWhO82k0bhcgMfVSTREEo1tugUMeFNoFOk0JlTkVZoWVTQm3LBV",
        "mhZVNHwxdAqNimLZZX8GuqSbksLyYzV6DVbVYYzq9Xp+DWU6UljAbWUwLNLWJciihutvMSw81RJJLOD7EdFnTbwFLeE0tcVh2D+G1o/aAuZlJO4ssvzqdo/d/r4Kq3Tj",
        "GG5vB8xHuLd3JK+zImxCXRKsjXcUjyKZhzsXY3jIZB7OihzFQ2lL56XfII9MOneRLsTtZjl8bLhVwBx5G2nSZZb2fo30VWq1U8vy5Lmtkw605SdPZmHUd25GcYEmJplL",
        "Qy8YA4P+Vhz3XBUZkMikkfTg+qpIQzqtC5UoRQN/PwfiMzY+DqPmgV4uBqbxR3EgvV4jvZchRnHQ+jj8YAaYCvHlANOplIit6jENi/nV10m5pL4HJjwu3CgyNeIbUUJx",
        "gdBhHipN+rxMvmD6pjVpbEAXdxyb1KIyfoA10zexSSuJAEOhcWwE2EMkcCTkHAiBy4fH3aWW0fY06KXMm+pqcB40ZAi3CX9shm2k5OGnayJM7djSW0BFG+ITR5Fr5TPf",
        "feshwWJLsHophk/X9n//6+r603p1kV1eaqolUADzF6GqCUL9x/5cgkw/hto32yHV0oMVXYpN+CiwMG82GBNEb6gD3podx4IR34EY6av6WJCuWPhPikfOBfywWchXBKv6",
        "I0elIQaYRx1g9+zLw/yLk4gfOR+p19oCjgjrHgJJ5SMTi7i59wxrxKfSnjS91Q9Xq2bhZKRHfiutbu1ci2VaD6xqRXHhYac0roSLfmanq28P68X9zeZRLcOr4Ys0Blml",
        "mJ1n0g3TpZkdbirlsJ+SvoCbA/YUZOVwgu5QpnzesoInTF+Q+3Op5R5qHPDocNQOuSkgj05frhPBWXwsj0kTKsj1Ix4Kce9cDKdo2OZnsqwJz+K5VotG19Ws5wI3knhE",
        "LppGhUjsQzbHd/TGM5FGJK1+Qp+DQkWHqWsEfr4HxcO/XeJGWMDaRiRUnEFuLMIrGdyKyLuLJUwTIGaefQSUSLjFMIIEPfUMhlfjSNBzz7fr+cm7aUkQn073VeCNI0G8",
        "U+e7QzWOBPFSnS/MHEeC+MIE98R340jQXz1rd8fR+2lJkO7V+S8rjyKhL9Zx0vFwq7p9VQtpXDQTlpT8PwwXjpgVCpdo6XZhlqu3/9BdDvUdu8efFZAL3ReFLkB2R+6x",
        "+pp9JKF+W8PLSUqpk+FLMjzUdyMZnlRupuGX87e2LU2GrymlRQH46DaAwIlXGDzWM3XsntfQg3YTiKmT4VlKCOjYiWR4pQudhqNh+KmwBXnokLOQDE9SeN5b58nwJIXn",
        "9VKS4UkKT/raPiXDE+trPdftk+FrMvzt/fzE7meTDE9SeVv4l1OpvJKk8hQ81HciGZ6k8hT8lLaWdmlL+trYJsOTtJ6GX83fvJ0C/o+7Z62v+eF2uVmoBsdMbhscl7z/",
        "+dMmq/d0dor3XOv9k83y7v7mKc/qrGl2SGyXnukklV5JajDnu8sTA2FMMFda952a3JSmLlSatylbgcYDLN9/W/y23Kwuvz7pEg4DmCor+RAlV0FS9zlK4UXRPcbPO28z",
        "KqpAiuouKUREdcahlIFvAs8kLKxEC7u8jgqrYqtFC7v7LSqpxtHq3Z2IsAZH6/IuKokh9wNibalD94AolQA570xbXFS3G2QT21txOSJGCT9RutbITlFHU6G7unPe7nvj",
        "DQzOxGySFa/OsWtP8mUo7no9eJGjHJTAN6xlNHuz2iw/3/z+VGQyKzW12R6kRIJcLUeAVL1O3YJ0av37y5tvt4v71cW6lXx8+vb1rO4WpQ3dVQba4qBNByvV8PrW+r5B",
        "arzudlhEljp9TdBSZSYqQFpk522lIQapDjgRojrbGxMV2XxbUd3pTUxUgdaeP7yMS5Nhy91Km/3er4tl0EZocWWCS1FlogFERezNVl9ZKhQU1a392j9lW1HHH1+0/1vO",
        "vr94WK2vZjmvC/4kpqQ1QsQKbREQ20EddIWVS977JUnKRYOwCIjcbZMRIBwJkqgmNYjaWQ1KTQLqrlMDtsQiXfF2Dp4tTiaKy7XraYsr08V1u84Wl2ZldGQKzF2doH1b",
        "RT4IAvKs0qKa6OrZ+lD01bMFUeckjceRsSKXy+sROCystLdjAWxcPpyaUhUsFRHDtJW2RIgSFNVY7lSj4FVtq0YPQoFTjfaSMEWpC2jqqMD5WrATk6JTdjjlKJ0y8L52",
        "Ekc4c4NduxNXI92v8KTq2LUBPABIVn+XLyJOpntzkDSkNxde6VpU1JvLXRPtERUNpYDoziOq2x1d8SYkqqn7pRwX0+2MyloQg9NE8yixD4DiUkvkMjOmvx19ZchSGlBG",
        "0wc5gpYWVRsrVv3cr7Tj4hqkODsfAYtTOVZyaCS7F+kGsqSqEyyhvBr8AUxn3xGnVLFKgB7U2vU4KtOZI4C2Yvo+GiMgCypkoa/Dj4CUVEgxGjLN+ZP6gssTRxwypWek",
        "LZ2lpSxSiXT8bE3BDVE6aRnN6OVuogoWVUUyeqCi9ohigKKOqEFITGwLGuF70krROMqvs6slHb4jALZuHWIpmo92PnFEJea8nRlW+rdKTHp7pGGz3l8NwwCKQm4R47zB",
        "w6qJjTEnjLGO7hJgjB5R0bw3kLTxiFJ5bzYwyxKX6OJ954xuB1SVSeW0tc6z0399u/hw+rxfhmeLdrXeP21YBWa5aiM0Vb1XVS+aqgas8jnwQO0g6S0MlkwOJeuTrxra",
        "CnCAY7gPHmnRbBxzHUyPqAoYM7Di9jn9vLtTZgYgHtHRfQHkDD2ikGk4xIAb5KYwrJBHlEq22fdkgBHeb/rFUmdsoI3LVhvXO23Ms+4B250K7SFUcFPFtHFpRax6mRrx",
        "ai9PoByNEsoy3W9cccpYQDvGWdbubjantD/0w++T4LLRWasGMhm+tEJcWoUMAAwl6JFlhjqBQF9XicTEYY9tgtu2PwLKsYHO9QVCWGqk45PHU0JXnzBlSlhc2KvlxWpx",
        "jZw+bNEAYsnp7goJTo9PHNJwGMdVPllIDwr1IZAuFGrKUgoHPLKwlQOYMcZKB7ayMGNkKYlinywRS2GTlhhDpoWjSvKPP/4P");
        return unserialize(gzinflate(base64_decode(join($ebsa))));
    }

    public function encode($string) {
        return rawurlencode(utf8_encode($string));
    }
}
?>
