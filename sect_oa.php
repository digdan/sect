<?php
/**
*    SeCT_OA - cURL wrapper with 2Legged OAuth
*/

class sect_oa extends sect {
	private $use_oa=false;
	private $oa_consumerKey;
	private $oa_consumerSecret;
	private $oa_signatureMethod;
	private $oa_version = '1.0';

	public function __construct($options=array()) {
		parent::__construct();
		if (count($options) > 0) { //Passed OAuth options
			$this->oa($options["consumerKey"],$options["consumerSecret"],$options["signatureMethod"]);
		}
	}

    public function oa($consumerKey, $consumerSecret, $signatureMethod='HMAC-SHA1') {
        $this->oa_consumerKey = $consumerKey;
        $this->oa_consumerSecret = $consumerSecret;
        $this->oa_signatureMethod = $signatureMethod;
        $this->use_oa = true;
    }

    //Sign string
    private function oa_sign($string = null)  {
        $retval = false;
        switch($this->oa_signatureMethod) {
            case 'HMAC-SHA1':
                $key = $this->encode($this->oa_consumerSecret)."&";
                $retval = base64_encode(hash_hmac('sha1',$string,$key,true));
                break;
        }
        return $retval;
    }

    //Add OAuth headers to next request
    private function oa_headers($url,$headers=NULL)  {
        $_h = array();
        $oauth_a = array();
        $urlParts = parse_url($url);
        $oauth = 'Authorization: OAuth realm="",';
        foreach($headers as $name => $value)    {
            $oauth_a[] = "{$name}=\"{$value}\"";
        }
        ksort($oauth_a);
        $_h[] = $oauth.join($oauth_a,",");
        curl_setopt($this->ch, CURLOPT_HTTPHEADER, $_h);
    }

    //Nonce
    private function oa_nonce()  {
        if(isset($this->oa_nonce)) // for unit testing
            return $this->oa_nonce;
        return md5(uniqid(rand(), true));
    }

    //Create OAuth URL Signature
    private function oa_signature($method = null, $url = null, $params = null) {
        if(empty($method) || empty($url))
            return false;

        if ((is_null($params)) or ( ! $params)) $params = array();

        // concatenating
        $concatenatedParams = '';

        foreach($params as $k => $v) {
                $v = $this->encode($v);
                $concatenatedParams .= "{$k}={$v}&";
        }

        $concatenatedParams = $this->encode(substr($concatenatedParams, 0, -1));

        $normalizedUrl = $this->encode($this->normalizeUrl($url));
        $method = $this->encode(strtoupper($method)); // don't need this but why not?

        $signatureBaseString = "{$method}&{$normalizedUrl}&{$concatenatedParams}";

        return $this->oa_sign($signatureBaseString);
    }

    private function oa_digest($method = null, $url = null, $params = null) {
        if(is_null($method) || is_null($url))
            return false;

        if ((is_null($params)) or ( ! $params)) $params = array();

        $oauth['oauth_consumer_key'] = $this->oa_consumerKey;
        $oauth['oauth_signature_method'] = $this->oa_signatureMethod;
        $oauth['oauth_timestamp'] = !isset($this->timestamp) ? time() : $this->timestamp; // for unit test
        $oauth['oauth_nonce'] = $this->oa_nonce();
        $oauth['oauth_version'] = $this->oa_version;

        // encoding

        array_walk($oauth, array($this, 'encode'));
        if(is_array($params))
            array_walk($params, array($this, 'encode'));

        $encodedParams = array_merge($oauth, $params);

        // sorting
        ksort($encodedParams);

        // signing
        $oauth['oauth_signature'] = $this->encode($this->oa_signature($method, $url, $encodedParams));

        $this->oa_headers($url,$oauth);
        return true;
    }
/**
* Send a HTTP GET request
*
* @param mixed $url - Url to GET from
* @param mixed $inarr - Array of values to send in GET request
*/
    public function get($url, $inarr = false) {
        ob_start();
        $this->timestamp = time();
        $this->set_url($url);
        if ($this->use_oa) $this->oa_digest('GET',$this->url,$inarr); //OAuth

        $this->return = curl_exec($this->ch);
        $this->info = curl_getinfo($this->ch);
        $this->error = curl_error($this->ch);

        if (!$this->return) {
            echo curl_error($this->ch);
        }
        $this->headers();
        ob_end_clean();
        return $this->return;
    }

/**
* Send a HTTP POST request
*
* @param mixed $url - The URL to post
* @param mixed $inarr - The array of values to post
*/
    public function post($url, $inarr = false) {
        ob_start();
        $this->timestamp = time();
        $this->set_url($url);
        if ($this->use_oa) $this->oa_digest('POST',$this->url,$inarr); //OAuth
        curl_setopt($this->ch, CURLOPT_POST, true);
        if (!$inarr) {
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, "");
            unset($this->last_post);
        } else {
            $this->last_post = $inarr;
            $post_string = http_build_query($inarr);
            curl_setopt($this->ch, CURLOPT_POSTFIELDS, $post_string);
        }
        $this->return = curl_exec($this->ch);
        $this->info = curl_getinfo($this->ch);
        $this->error = curl_error($this->ch);
        if (!$this->return) {
            echo curl_error($this->ch);
        }
        $this->headers();
        ob_end_clean();
        return $this->return;
    }

}
?>
